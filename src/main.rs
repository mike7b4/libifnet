use std::convert::TryFrom;
use std::os::unix::io::AsRawFd;
use std::cell::RefCell;
use std::rc::Rc;
use std::net::{
    UdpSocket,
    SocketAddr,
    Ipv4Addr,
    IpAddr
};
use std::ffi::{CStr, CString};
use libc::{
    SIOCGIFCONF,
    SIOCGIFHWADDR,
    SIOCGIFNAME,
    IFNAMSIZ,
    AF_INET,
    sockaddr_in,
    in_addr,
};
use nix::ioctl_read_bad;

mod IfNet {
    #[derive(Debug)]
    pub enum Error {
        InterfaceNotFound,
        NotIpv4,
    }
}

const SIOCGIFINDEX: i32 = 0x8933;
#[repr(C)]
#[derive(Debug)]
pub struct IfMap {
    mem_start: libc::c_ulong,
    mem_end: libc::c_ulong,
    base_addr: libc::c_ushort,
    irq: libc::c_char,
    dma: libc::c_char,
    port: libc::c_char,
}
/*
#[repr(C)]
union U {
    addr: sockaddr,
    dstaddr: sockaddr,
    broadaddr: sockaddr,
    netmask: sockaddr,
    hwaddr: sockaddr,
    flags:  libc::c_short,
    ifindex: libc::c_int,
    metric: libc::c_int,
    mtu: libc::c_int,
    map: IfMap,
    slave: [libc::c_char; IFNAMSIZ],
    new_name: [libc::c_char; IFNAMSIZ],
    data: *mut u8,
}
*/
#[repr(C)]
#[derive(Debug)]
pub struct CIfReq<T> {
    name: [libc::c_char; IFNAMSIZ],
    payload: T,
    _pad: [u8; 8],
}

#[derive(Debug)]
pub struct IfReq {
    name: String,
    addr: std::net::Ipv4Addr,
}

impl IfReq {
    fn new(name: String, addr: Ipv4Addr) -> Self {
        Self {
            name,
            addr
        }
    }
}

#[repr(C)]
#[derive(Debug)]
pub struct IfReqs {
    len: libc::c_int,
    buf: *mut u8,
}

impl IfReqs {
    fn new(len: libc::c_int, buf: *mut u8) -> Self {
        println!("{}", len); 
        Self {
            len,
            buf
        }
    }
}

impl From<libc::c_int> for CIfReq<libc::c_int> {
    fn from(c: libc::c_int) -> Self {
        Self {
            name: [0; 16],
            payload: c,
            _pad: [0; 8],
        }
    }
}

impl TryFrom<String> for CIfReq<libc::c_int> {
    type Error = IfNet::Error;
    fn try_from(c: String) -> Result<Self, IfNet::Error> {
        if c.len() > 15 {
            return Err(IfNet::Error::InterfaceNotFound);
        }
        let mut bytes = String::with_capacity(16);
        bytes += &c;
        bytes.truncate(15);
        let mut ifr = Self {
            name: [0; 16],
            payload: 0,
            _pad: [0; 8],
        };

        let mut i = 0;
        for byte in bytes.as_bytes().iter() {
            ifr.name[i] = *byte as i8;
            i += 1;
        }
        Ok(ifr)
    }
}

impl From<String> for CIfReq<libc::sockaddr> {
    fn from(c: String) -> Self {
        let mut bytes = String::with_capacity(16);
        bytes += &c;
        bytes.truncate(15);
        let mut ifr = Self {
            name: [0; 16],
            payload: unsafe { std::mem::zeroed() },
            _pad: [0; 8],
        };

        let mut i = 0;
        for byte in bytes.as_bytes().iter() {
            ifr.name[i] = *byte as i8;
            i += 1;
        }
        ifr
    }
}

impl TryFrom<&CIfReq<libc::sockaddr>> for IfReq {
    type Error = IfNet::Error;
    fn try_from(c: &CIfReq<libc::sockaddr>) -> Result<Self, IfNet::Error> {
        let ip = unsafe {
            // stolen from Rust std lib API and modified for in_addr cast
            // what it does is get pointer to sa_data[14] array cast it to in_addr
            // similar to what C does
            if c.payload.sa_family != AF_INET as u16 {
                return Err(IfNet::Error::NotIpv4);
            }
            let ip = *(&c.payload as *const libc::sockaddr as *const libc::sockaddr_in);
            let ip: Ipv4Addr = *(&ip.sin_addr as *const libc::in_addr as *const Ipv4Addr);
            ip
        };
        let c_str: &CStr = unsafe { CStr::from_ptr(c.name.as_ptr()) };
        let name = c_str.to_str().unwrap_or("").to_string().to_owned();
        Ok(Self {
            name,
            addr: ip,
        })
    }
}

// sockaddr addr
ioctl_read_bad!(read_if_conf, SIOCGIFCONF, IfReqs);
// ifindex
ioctl_read_bad!(read_if_hwaddr, SIOCGIFHWADDR, CIfReq<libc::sockaddr>);
// ifindex
ioctl_read_bad!(read_if_name, SIOCGIFNAME, CIfReq<libc::c_int>);
// ifindex
ioctl_read_bad!(read_if_index, SIOCGIFINDEX, CIfReq<libc::c_int>);
fn main() {
    let iwsock = UdpSocket::bind("127.0.0.1:0").unwrap();
    let array: Vec<CIfReq<libc::sockaddr>> = Vec::with_capacity(20);
    let array = Rc::new(RefCell::new(array));
    let ptr = array.borrow_mut().as_mut_ptr() as *mut u8;
    let mut wifreqs = IfReqs::new(20 * std::mem::size_of::<CIfReq<libc::sockaddr>>() as i32, ptr);
    unsafe {
        match read_if_conf(iwsock.as_raw_fd(), &mut wifreqs) {
            Ok(_) => {
                // Kernel filling len with the new value
                let len = wifreqs.len as usize/std::mem::size_of::<CIfReq<libc::sockaddr>>();
                array.borrow_mut().set_len(len);
                Ok(len)
            },
            Err(e) => {
               Err(e) 
            }
        }
    }.expect("Bailed");
    let array = array.borrow_mut();
    let mut ifs: Vec<IfReq> = Vec::new();
    if array.len() > 0 {
        for data in array.iter() {
            ifs.push(IfReq::try_from(data).expect("bailed"));
        }
    }

    let mut cif: CIfReq<libc::c_int> = CIfReq::from(2);
    unsafe {
        match read_if_name(iwsock.as_raw_fd(), &mut cif) {
            Ok(len) => {
                // Kernel filling len with the new value
                //let len = wifreqs.len as usize/std::mem::size_of::<CIfReq>();
                println!("len {:?}", cif.name);
                Ok(len)
            },
            Err(e) => {
                println!("{}", e);
               Err(e) 
            }
        }
    }.expect("bailed");
         
    for ref iface in ifs {
        let if_name = String::from(iface.name.clone());
        let mut cif: CIfReq<libc::c_int> = CIfReq::try_from(String::from(&if_name)).expect("Get iface bailed");
        let if_index = unsafe {
            match read_if_index(iwsock.as_raw_fd(), &mut cif) {
                Ok(_) => {
                    cif.payload
                },
                Err(e) => {
                    eprintln!("{}", e);
                    -1
                }
            }
        };
        println!("if_index {}", if_index);
        println!("address: {}", iface.addr);

        let mut cif: CIfReq<libc::sockaddr> = CIfReq::from(String::from(&if_name));
        let hwaddr = unsafe {
            match read_if_hwaddr(iwsock.as_raw_fd(), &mut cif) {
                Ok(_) => {

                    let mut hw = String::new();
                    for byte in cif.payload.sa_data[0..6].iter() {
                        hw += &format!("{:02X}:", byte);
                    }
                    hw.truncate(17);
                    hw
                },
                Err(e) => {
                    eprintln!("{}", e);
                    String::from("")
                }
            }
        };
        println!("{}: {}", &if_name, hwaddr);
    }
}

mod tests {
    use crate::*;
    #[test]
    fn test_iface_from_string() {
        assert_eq!(CIfReq::<libc::c_int>::try_from(String::from("eth0")).is_ok(), true);
    }

    #[test]
    fn test_iface_from_long_string_should_fail() {
        assert_eq!(CIfReq::<libc::c_int>::try_from(String::from("eth1234567890123456")).is_err(), true);
    }
}
